﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Xml;

namespace Common
{
    public class Define
    {
        static Define _instance;
        public static Define Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Define();
                }
                return _instance;
            }
        }

        public string RootPath { get; set; } = string.Empty;
        public string ConnectionString { get; set; } = string.Empty;
        public int PageSize { get; private set; } = 10;
        public string GoogleClientID { get; set; } = string.Empty;
        public string GoogleClientSecret { get; set; } = string.Empty;

        public enum fLogin
        {
            Fail = 0,
            NotFound = 1,
            Disable = 2,
            Success = 3
        }
        public enum fStatus
        {
            Add = 1,
            Edit = 2
        }
        public enum fLog
        {
            Trace = 1,
            Debug = 2,
            Info = 3,
            Warn = 4,
            Error = 5,
            Fatal = 6
        }

        public string MSGGetAll { get; set; } = "[-----Tất cả-----]";
        public string MSGFail { get; set; } = "Fail";
        public string loginSession { get; set; } = "loginsession";
        public string AspDotNet_UserId { get; } = "ASP.NET_UserId";
    }
}
