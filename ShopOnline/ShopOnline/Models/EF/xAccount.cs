using ShopOnline.Models.General;
using ShopOnline.Models.Interface;

namespace ShopOnline.Models.EF
{
    public partial class xAccount : Master, IPersonnel, IPermissionGroup
    {
        public string IPAddress { get; set; } = string.Empty;
        public int IDPersonnel { get; set; }
        public string PersonnelCode { get; set; }
        public string PersonnelName { get; set; }
        public int IDPermissionGroup { get; set; }
        public string PermissionGroupCode { get; set; }
        public string PermissionGroupName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsEnable { get; set; }
    }
}
