﻿using ShopOnline.Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    public class MenuGroupController : BaseController<xMenuGroup>
    {
        public MenuGroupController(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ActionResult ShowMenu()
        {
            return PartialView(Instance.GetRepository<xMenuGroup>().GetItems());
        }
    }
}