﻿using Common;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Google.Apis.Util;
using Google.Apis.Util.Store;
using ShopOnline.BLL;
using ShopOnline.GoogleAPI;
using ShopOnline.Models.EF;
using ShopOnline.Models.OtherEF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    public class AccountController : BaseController<xAccount>
    {
        public AccountController(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override ActionResult Index()
        {
            return View(new LoginRequest());
        }

        public async  Task<ActionResult> Login()
        {
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(CancellationToken.None);
            return View(new LoginRequest());
        }

        [HttpPost]
        public ActionResult NormalAuthorization(LoginRequest login)
        {
            if (login == null)
                return BadRequest(Define.fLogin.Fail.ToString(), "Login Fail.");

            Dictionary<string, object> models = new Dictionary<string, object>();

            bool isValid = true;
            if (login.Username.IsEmpty())
            {
                models.Add(nameof(login.Username), "Username is required.");
                isValid = false;
            }

            if (login.Password.IsEmpty())
            {
                models.Add(nameof(login.Password), "Password is required.");
                isValid = false;
            }

            if (!isValid)
                return Ok(models);

            Define.fLogin res = Instance.CheckLogin(login.Username, login.Password);
            switch (res)
            {
                case Define.fLogin.Success:
                    models.Add(res.ToString(), Url.Action("Index", "Admin", null));
                    break;
                case Define.fLogin.NotFound:
                    models.Add(res.ToString(), "Username or password is wrong.");
                    break;
                case Define.fLogin.Disable:
                    models.Add(res.ToString(), "Account is disabled.");
                    break;
            }
            return Ok(models);
        }

        public async Task<ActionResult> GoogleSignIn()
        {
            try
            {
                UserCredential credential = null;
                string redirectUri = string.Empty;

                HttpCookie cookieRequest = Request.Cookies[Define.Instance.AspDotNet_UserId];
                if (cookieRequest != null)
                    credential = await LoadExistToken(cookieRequest.Value);


                if (credential == null)
                {
                    var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(CancellationToken.None);
                    credential = result.Credential;
                    redirectUri = result.RedirectUri;
                }

                if (credential != null)
                {
                    Response.Cookies[Define.Instance.AspDotNet_UserId].Value = credential.UserId;
                    Response.Cookies[Define.Instance.AspDotNet_UserId].Expires = DateTime.Now.AddSeconds(credential.Token.ExpiresInSeconds ?? 0);
                }
                else
                {
                    Response.Cookies[Define.Instance.AspDotNet_UserId].Value = string.Empty;
                    Response.Cookies[Define.Instance.AspDotNet_UserId].Expires = DateTime.Now.AddSeconds(-1);
                }

                if (credential != null)
                {
                    if (credential.Token.IsExpired(credential.Flow.Clock))
                    {
                        TokenResponse response = await credential.Flow.RefreshTokenAsync(credential.UserId, credential.Token.RefreshToken ?? credential.Token.AccessToken, CancellationToken.None);
                    }

                    DriveService service = new DriveService(new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "ASP.NET MVC Sample"
                    });

                    FilesResource.ListRequest listRequest = service.Files.List();
                    //listRequest.Q = "fileExtension='null'";
                    var files = await listRequest.ExecuteAsync();
                    ViewBag.Items = files.Items.ToList();
                    return RedirectToAction("Index");
                }
                else
                {
                    return new RedirectResult(redirectUri);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> GoogleSignOut()
        {
            try
            {
                UserCredential credential = null;
                HttpCookie cookieRequest = Request.Cookies[Define.Instance.AspDotNet_UserId];
                if (cookieRequest != null)
                    credential = await LoadExistToken(cookieRequest.Value);

                if (credential != null)
                {
                    await RevokeAccount(credential);

                    DriveService service = new DriveService(new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "ASP.NET MVC Sample"
                    });
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
            return RedirectToAction("Index");
        }

        // async Task RevokeToken(string userId)
        //{
        //    var typeName = typeof(TokenResponse).FullName;

        //    var context = this.DbContext;
        //    var tokens = await context.GoogleAuthData
        //        .Where(o => o.UserId == userId && o.Type == typeName)
        //        .ToArrayAsync();

        //    foreach (var item in tokens)
        //    {
        //        try
        //        {
        //            var token = JsonConvert.DeserializeObject<TokenResponse>(item.Value);
        //            var flow = GmailApiComponent.CreateFlow(userId);
        //            var userCredential = new Google.Apis.Auth.OAuth2.UserCredential(flow, userId, token);

        //            //Refresh the access token so we can promptly destroy it.
        //            await userCredential.RefreshTokenAsync(CancellationToken.None);
        //            await userCredential.RevokeTokenAsync(CancellationToken.None);
        //        }
        //        catch
        //        {
        //            //TODO: Logging!
        //        }
        //    }
        //}

        async Task<bool> RevokeAccount(UserCredential credential)
        {
            try
            {
                credential.Token.ExpiresInSeconds = 0;
                if (credential.Token.RefreshToken.IsNotEmpty())
                    await RevokeToken(credential.Token.RefreshToken);
                await RevokeToken(credential.Token.AccessToken);

                FileDataStore ds = new FileDataStore(GoogleWebAuthorizationBroker.Folder);
                await ds.DeleteAsync<TokenResponse>(credential.UserId);
                Response.Cookies[Define.Instance.AspDotNet_UserId].Expires = DateTime.Now.AddSeconds(-1);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
        }

        async Task RevokeToken(string token)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                using (HttpRequestMessage request = new HttpRequestMessage())
                {
                    request.Content = new StringContent(string.Empty);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    request.Method = HttpMethod.Get;

                    await client.GetAsync("https://accounts.google.com/o/oauth2/revoke?token=" + token);
                }
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
        }

        async Task<UserCredential> LoadExistToken(string userId)
        {
            try
            {
                IAuthorizationCodeFlow flow = new AppFlowMetadata().Flow;
                TokenResponse token = await flow.LoadTokenAsync(userId, CancellationToken.None);
                if (token != null)
                    return new UserCredential(flow, userId, token);
            }
            catch (Exception ex)
            {
                Log.Error(MethodBase.GetCurrentMethod(), ex);
            }
            return null;
        }
    }
}