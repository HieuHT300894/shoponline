﻿using Common;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Google.Apis.Util;
using ShopOnline.BLL;
using ShopOnline.GoogleAPI;
using ShopOnline.Models.EF;
using ShopOnline.Models.OtherEF;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ShopOnline.Controllers
{
    public class LoginController : BaseController<xAccount>
    {
        public LoginController(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override ActionResult Index()
        {
            return View(new LoginRequest());
        }

        [HttpPost]
        public ActionResult NormalAuthorization(LoginRequest login)
        {
            if (login == null)
                return BadRequest(Define.fLogin.Fail.ToString(), "Login Fail.");

            Dictionary<string, object> models = new Dictionary<string, object>();

            bool isValid = true;
            if (login.Username.IsEmpty())
            {
                models.Add(nameof(login.Username), "Username is required.");
                isValid = false;
            }

            if (login.Password.IsEmpty())
            {
                models.Add(nameof(login.Password), "Password is required.");
                isValid = false;
            }

            if (!isValid)
                return Ok(models);

            Define.fLogin res = Instance.CheckLogin(login.Username, login.Password);
            switch (res)
            {
                case Define.fLogin.Success:
                    models.Add(res.ToString(), Url.Action("Index", "Admin", null));
                    break;
                case Define.fLogin.NotFound:
                    models.Add(res.ToString(), "Username or password is wrong.");
                    break;
                case Define.fLogin.Disable:
                    models.Add(res.ToString(), "Account is disabled.");
                    break;
            }
            return Ok(models);
        }

        public async Task<ActionResult> GoogleAuthorization(CancellationToken cancellationToken)
        {
            var result = await new AuthorizationCodeMvcApp(this, new AppFlowMetadata()).AuthorizeAsync(cancellationToken);

            if (result.Credential != null)
            {
                if (result.Credential.Token.IsExpired(SystemClock.Default))
                {
                    TokenResponse response = await result.Credential.Flow.RefreshTokenAsync(result.Credential.UserId, result.Credential.Token.RefreshToken ?? result.Credential.Token.AccessToken, CancellationToken.None);
                }
                DriveService service = new DriveService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = result.Credential,
                    ApplicationName = "ASP.NET MVC Sample"
                });
                var list = await service.Files.List().ExecuteAsync();
                ViewBag.Message = "FILE COUNT IS: " + list.Items.Count;
                return RedirectToAction("Index", "Login");
            }
            else
            {
                return new RedirectResult(result.RedirectUri);
            }
        }
    }
}