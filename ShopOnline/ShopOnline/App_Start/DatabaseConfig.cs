﻿using Common;
using ShopOnline.Models.Model;
using System.Configuration;
using System.Data.Entity;

namespace ShopOnline
{
    public class DatabaseConfig
    {
        public static void GetConnectionString()
        {
            ConnectionStringSettings setting = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            Define.Instance.ConnectionString = setting != null ? setting.ConnectionString : string.Empty;

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<zModel, CustomConfiguration>());
            zModel db = new zModel();
            db.Database.Initialize(false);
        }
    }
}