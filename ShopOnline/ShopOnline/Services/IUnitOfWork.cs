﻿using System.Threading.Tasks;

namespace ShopOnline
{
    public interface IUnitOfWork
    {
        void BeginTransaction();
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void CommitTransaction();
        void RollbackTransaction();
    }
}